import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { ProductBrandComponent } from './product-brand/product-brand.component';


@NgModule({
  declarations: [
    AppComponent,
    MenuListComponent,
    ProductBrandComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
