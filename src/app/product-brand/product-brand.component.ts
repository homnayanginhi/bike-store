import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-product-brand',
  templateUrl: './product-brand.component.html',
  styleUrls: ['./product-brand.component.css']
})
export class ProductBrandComponent implements OnInit {
@Input() productBrand: any;
  constructor() { }

  ngOnInit() {
  }

}
