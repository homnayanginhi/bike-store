import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  products: any;

  constructor() {
    this.products = [
      {
        name: 'Xe', items: [
          {
            name: 'Xe Đạp', items: [
              { name: 'Giant' },
              { name: 'Cannondale' }
            ]
          },
          {
            name: 'Xe Tay Ga', items: [
              { name: 'Honda' },
              { name: 'Yamaha' },
              { name: 'Suzuki' }
            ]
          },
          {
            name: 'Xe Tay Côn', items: [
              { name: 'Honda' },
              { name: 'SYM' },
              { name: 'Suzuki' }
            ]
          }
        ]
      },
      {
        name: 'Điện Thoại', items: [
          {
            name: 'Điện Thoại Bình Dân', items: [
              { name: 'Nokia' },
              { name: 'Philips' }
            ]
          },
          {
            name: 'Điện Thoại Cao Cấp', items: [
              { name: 'Apple' },
              { name: 'Samsung' },
              { name: 'Google' },
              { name: 'LG' }
            ]
          }
        ]
      }
    ];
  }
}
